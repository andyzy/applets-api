package com.shz.appletsapi.prop;

public interface ShzWebMsg {
	
	String MSG_SUCCESS = "操作成功";
	String MSG_FAILURE = "操作失败";
	
	String MSG_IDNULL="id不能为空";
	String MSG_STATUSNULL="状态不能为空";
	String MSG_APPCODENULL="密匙不能为空";
	String MSG_HEADIMGURLNULL="头像地址不能为空";
	String MSG_USERNAMENULL="用户名不能为空";

}
