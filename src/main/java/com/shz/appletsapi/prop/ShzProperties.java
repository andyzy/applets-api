package com.shz.appletsapi.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;


@Configuration
@ConfigurationProperties("shz")
public class ShzProperties {

	@Getter
	private final Async async = new Async();
	
	
	/**
	 * 异步线程设置
	 */
	@Getter
	@Setter
	public static class Async {
		private int corePoolSize = ShzDefaults.Async.corePoolSize;
		private int maxPoolSize = ShzDefaults.Async.maxPoolSize;
		private int queueCapacity = ShzDefaults.Async.queueCapacity;
		private int keepAliveSeconds = ShzDefaults.Async.keepAliveSeconds;
	}

}
