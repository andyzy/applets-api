package com.shz.appletsapi.prop;



public interface ShzDefaults {

	interface Async {
		int corePoolSize = 2;
		int maxPoolSize = 50;
		int queueCapacity = 10000;
		int keepAliveSeconds = 300;
	}


}
