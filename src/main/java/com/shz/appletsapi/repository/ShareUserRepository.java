package com.shz.appletsapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.ShareUser;

public interface ShareUserRepository  extends CrudRepository<ShareUser, Integer> {
	

}
