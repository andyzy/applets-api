package com.shz.appletsapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.Menu;

public interface MenuRepository  extends CrudRepository<Menu, Integer> {

}
