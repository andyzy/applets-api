package com.shz.appletsapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.Banner;

public interface BannerRepository  extends CrudRepository<Banner, Integer> {

}
