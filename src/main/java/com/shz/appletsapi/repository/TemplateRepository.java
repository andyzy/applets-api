package com.shz.appletsapi.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.Template;

public interface TemplateRepository  extends CrudRepository<Template, Integer> {
	
	List<Template> findByMenuId(Integer menuId);
	
	Template findByTitle(String title);

}
