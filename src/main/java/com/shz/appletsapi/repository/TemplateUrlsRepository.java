package com.shz.appletsapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.TemplateUrls;

public interface TemplateUrlsRepository  extends CrudRepository<TemplateUrls, Integer> {

	
	TemplateUrls findByTemplateId(Integer templateId);
	
}
