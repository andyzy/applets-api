package com.shz.appletsapi.interceptor;

import org.apache.commons.lang.StringUtils;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


/**
 * 启动事件
 * @author zy
 *
 */
@Slf4j
@Component
public class StartEventListener {

	@Async
	@Order
	@EventListener(EmbeddedServletContainerInitializedEvent.class)
	public void afterStart(EmbeddedServletContainerInitializedEvent event) {
		String[] profiles = event.getApplicationContext().getEnvironment().getActiveProfiles();
		String profile = StringUtils.join(profiles);
		log.info("----项目启动完成，当前使用的环境变量:[{}]----", profile);
	}
	
}
