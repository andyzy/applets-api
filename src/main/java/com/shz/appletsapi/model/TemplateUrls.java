package com.shz.appletsapi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name = "template_urls")
public class TemplateUrls implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 7405249763527886170L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "template_id")
	private Integer templateId;
	
	@Column(name = "urls")
	private String urls;
	
	@Column(name = "music_url")
	private String musicUrl;
	
	@Column(name = "create_time")
	private Date createTime;
	
	@Column(name = "update_time")
	private Date updateTime;
}
