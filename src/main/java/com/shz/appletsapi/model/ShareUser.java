package com.shz.appletsapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "share_user")
public class ShareUser {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "app_code")
	private String appCode;
	
	@Column(name = "head_img_url")
	private String headImgUrl;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "createTime")
	private Date createTime;
	

}
