package com.shz.appletsapi.model.vo;

import lombok.Data;

@Data
public class IdVo {
	private Integer id;

	public IdVo(Integer id) {
		super();
		this.id = id;
	}

	
	
}
