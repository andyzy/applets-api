package com.shz.appletsapi.model.vo;

import java.util.List;

import lombok.Data;


@Data
public class TemplateUrlsVo {
	
	private List<String> imgList;
	private String musicUrl;

}
