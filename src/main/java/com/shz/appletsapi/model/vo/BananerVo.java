package com.shz.appletsapi.model.vo;

import java.util.List;

import lombok.Data;

@Data
public class BananerVo {
	
	 private String title; 
	 private String bannerUrl;
	 private String templateUrl;
	 private Integer templateId;
	
}
