package com.shz.appletsapi.model.vo;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class IndexVo {
	
	private List<BananerVo> banners;
	private List<MenuVo> menus;
}
