package com.shz.appletsapi.model.vo;

import lombok.Data;

@Data
public class TemplateVo {
    private Integer templateId;
	private String title;
	private String imgUrl;
	private String templateUrl;
	
}
