package com.shz.appletsapi.model.vo;

import java.util.List;

import lombok.Data;

@Data
public class MenuVo {
	
	private String content;
	private String advertisingUrl;
	private Integer sorting;
	private List<TemplateVo> templates;
	
}
