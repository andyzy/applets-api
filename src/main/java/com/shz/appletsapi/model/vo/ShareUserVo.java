package com.shz.appletsapi.model.vo;

import lombok.Data;

@Data
public class ShareUserVo {

	private String appCode;

	private String headImgUrl;

	private String userName;

	private Integer status;

}
