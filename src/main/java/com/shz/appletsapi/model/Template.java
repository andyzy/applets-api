package com.shz.appletsapi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "template")
public class Template implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3776918146704427148L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "menu_id")
	private Integer menuId;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "template_url")
	private String templateUrl;
	
	@Column(name = "img_url")
	private String imgUrl;
	
	@Column(name = "createTime")
	private Date createTime;
	
	@Column(name = "updateTime")
	private Date updateTime;

}
