package com.shz.appletsapi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "banner")
public class Banner implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2706272337110550046L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "banner_url")
	private String bannerUrl;
	
	@Column(name = "template_url")
	private String templateUrl;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "createTime")
	private Date createTime;
	
	@Column(name = "updateTime")
	private Date updateTime;

}
