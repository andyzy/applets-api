package com.shz.appletsapi.common.aspect;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


@Aspect
@Component
@Slf4j
public class TimeAspect {
	
	@Around("execution(* com.shz.appletsapi.controller.HomeController.*(..))")
	public Object handleControllerMethod(ProceedingJoinPoint pr) throws Throwable{
		
		Object[] args =pr.getArgs();
		for(Object ob:args){
			System.out.println("arg:"+ob);
		}
		Long startTime=new Date().getTime();
		Object object=pr.proceed();
		log.info("aspect time:"+(new Date().getTime()-startTime));
	
		return object;
	}

}
