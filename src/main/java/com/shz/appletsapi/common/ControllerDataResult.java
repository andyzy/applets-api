package com.shz.appletsapi.common;

import com.shz.appletsapi.prop.ShzWebConstants;

public class ControllerDataResult extends ControllerResult {

	private Object data;
	
	public ControllerDataResult(Object data) {
		super(ShzWebConstants.STATUS_SUCCESS,null);
		this.data = data;
	}
	
	public ControllerDataResult(String status,String msg,Object data) {
		super(status,msg);
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
}
