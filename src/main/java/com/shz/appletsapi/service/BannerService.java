package com.shz.appletsapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.shz.appletsapi.model.vo.BananerVo;
import com.shz.appletsapi.repository.TemplateRepository;

@Service
public class BannerService {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	TemplateRepository templateRepository;
	
	public List<BananerVo> getBanners(){
		List<BananerVo> bananerVos =new ArrayList<>();
		String sql ="SELECT\r\n" + 
				"	title,\r\n" + 
				"	banner_url bannerUrl,\r\n" + 
				"	template_url templateUrl \r\n" + 
				"FROM\r\n" + 
				"	banner";
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
		if( list != null && !list.isEmpty()) {
		for(Map<String,Object> map : list) {
			BananerVo bananerVo =new BananerVo();
			bananerVo.setBannerUrl(String.valueOf(map.get("bannerUrl")));
			bananerVo.setTemplateUrl(String.valueOf(map.get("templateUrl")));
			bananerVo.setTemplateId(templateRepository.findByTitle(String.valueOf(map.get("title"))).getId());
			bananerVo.setTitle(String.valueOf(map.get("title")));
			bananerVos.add(bananerVo);
		  }
		}
		return bananerVos;
	}
	

}
