package com.shz.appletsapi.config;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.shz.appletsapi.prop.ShzProperties;

@Configuration
public class ExecutorConfiguration extends AsyncConfigurerSupport {

	@Autowired
	private ShzProperties shzProperties;

	@Override
	@Bean(name = "zyTaskExecutor")
	public Executor getAsyncExecutor() {
		// TODO Auto-generated method stub
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(shzProperties.getAsync().getCorePoolSize());
		executor.setMaxPoolSize(shzProperties.getAsync().getMaxPoolSize());
		executor.setQueueCapacity(shzProperties.getAsync().getQueueCapacity());
		executor.setKeepAliveSeconds(shzProperties.getAsync().getKeepAliveSeconds());
		executor.setThreadNamePrefix("zy-executor-");
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		return executor;
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		// TODO Auto-generated method stub
		return new SimpleAsyncUncaughtExceptionHandler();
	}

}
