package com.shz.appletsapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.shz.appletsapi.filter.RequestLoggingFilter;

@Configuration
public class FilterConfiguration {
	
	@Bean
	RequestLoggingFilter rquestLogginFilter() {
		RequestLoggingFilter f = new RequestLoggingFilter();
		f.setLogEnabled(true);
		f.setIncludeHeaders(true);
		f.setIncludeQueryString(true);
		f.setIncludePayload(true);
		f.setIncludeResponse(true);
		f.setMaxPayloadLength(200);
		return f;
	}

}
