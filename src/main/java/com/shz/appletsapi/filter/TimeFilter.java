package com.shz.appletsapi.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


//@Component
@Slf4j
public class TimeFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
		Long startTime=new Date().getTime();
		chain.doFilter(request, response);
		Long time = new Date().getTime()-startTime;
		log.info("time  as  :" +time);
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
		
	}

}
