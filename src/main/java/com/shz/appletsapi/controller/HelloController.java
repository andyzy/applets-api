package com.shz.appletsapi.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shz.appletsapi.repository.MenuRepository;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class HelloController {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	MenuRepository menuRepository;

	
	
	@GetMapping("/hello")
	public Object  hello() {
		log.info("hello");
		return menuRepository.findAll();
	}
	
	@GetMapping("/jdbc")
	public List<Map<String, Object>> jdbc() {
		return jdbcTemplate.queryForList("select * from offer");
	}
	
	
}
