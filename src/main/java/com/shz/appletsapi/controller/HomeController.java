package com.shz.appletsapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shz.appletsapi.common.ControllerDataResult;
import com.shz.appletsapi.common.ControllerResult;
import com.shz.appletsapi.model.Menu;
import com.shz.appletsapi.model.ShareUser;
import com.shz.appletsapi.model.Template;
import com.shz.appletsapi.model.TemplateUrls;
import com.shz.appletsapi.model.vo.IdVo;
import com.shz.appletsapi.model.vo.IndexVo;
import com.shz.appletsapi.model.vo.MenuVo;
import com.shz.appletsapi.model.vo.ShareUserVo;
import com.shz.appletsapi.model.vo.TemplateUrlsVo;
import com.shz.appletsapi.model.vo.TemplateVo;
import com.shz.appletsapi.prop.ShzWebConstants;
import com.shz.appletsapi.prop.ShzWebMsg;
import com.shz.appletsapi.repository.MenuRepository;
import com.shz.appletsapi.repository.ShareUserRepository;
import com.shz.appletsapi.repository.TemplateRepository;
import com.shz.appletsapi.repository.TemplateUrlsRepository;
import com.shz.appletsapi.service.BannerService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/applets")
@Slf4j
public class HomeController {
	
	
	@Autowired
	private BannerService bannerService;
	
	@Autowired
	private MenuRepository menuRepository;
	
	@Autowired
	private TemplateRepository templateRepository;
	
	@Autowired
	private ShareUserRepository shareUserRepository;
	
	@Autowired
	private TemplateUrlsRepository templateUrlsRepository;
	
	@GetMapping("/index")
	public ControllerResult index() {
		IndexVo indexVo=new IndexVo();
		List<MenuVo> menuVos = new ArrayList<MenuVo>() ;
		List<TemplateVo> templateVos = null;
		indexVo.setBanners(bannerService.getBanners());
		List<Menu> menus = (List<Menu>) menuRepository.findAll();
		for(Menu menu : menus) {
			MenuVo menuVo=new MenuVo();
			menuVo.setContent(menu.getContent());
			menuVo.setAdvertisingUrl(menu.getAdvertisingUrl());
			menuVo.setSorting(menu.getSorting());
			List<Template> templates = templateRepository.findByMenuId(menu.getId());
			templateVos=new ArrayList<>();
			for(Template template : templates) {
				TemplateVo templateVo =new TemplateVo();
				templateVo.setTemplateId(template.getId());
				templateVo.setTitle(template.getTitle());
				templateVo.setImgUrl(template.getImgUrl());
				templateVo.setTemplateUrl(template.getTemplateUrl());
				templateVos.add(templateVo);
			}
			menuVo.setTemplates(templateVos);
			menuVos.add(menuVo);
		}
		indexVo.setMenus(menuVos);
		return new ControllerDataResult(indexVo);
	}

	
	@PostMapping("/saveShareUser")
	public ControllerResult saveShareUser(@RequestBody ShareUser shareUser) {
		log.info("saveShareUser:"+shareUser.toString());
		Assert.notNull(shareUser.getAppCode(), ShzWebMsg.MSG_APPCODENULL);
		Assert.notNull(shareUser.getHeadImgUrl(), ShzWebMsg.MSG_HEADIMGURLNULL);
		Assert.notNull(shareUser.getUserName(), ShzWebMsg.MSG_USERNAMENULL);
		shareUser.setStatus(1);
		shareUser.setCreateTime(new Date());
		ShareUser su=shareUserRepository.save(shareUser);
		return new ControllerDataResult(ShzWebConstants.STATUS_SUCCESS,ShzWebMsg.MSG_SUCCESS,new IdVo(su.getId()));
	}
	
	@PostMapping("/getShareUser")
	public ControllerResult getShareUser(@RequestBody ShareUser shareUser) {
		log.info("getShareUser:"+shareUser.toString());
		Assert.notNull(shareUser.getId(), ShzWebMsg.MSG_IDNULL);
		ShareUser su =shareUserRepository.findOne(shareUser.getId());
		ShareUserVo suv=new ShareUserVo();
		suv.setAppCode(su.getAppCode());
		suv.setHeadImgUrl(su.getHeadImgUrl());
		suv.setUserName(su.getUserName());
		suv.setStatus(su.getStatus());
		return new ControllerDataResult(suv);		
	}
	
	@PostMapping("/updShareUserStatus")
	public ControllerResult updShareUserStatus(@RequestBody ShareUser shareUser) {
		Assert.notNull(shareUser.getId(), ShzWebMsg.MSG_IDNULL);
		Assert.notNull(shareUser.getStatus(), ShzWebMsg.MSG_STATUSNULL);
		ShareUser updshareUser = shareUserRepository.findOne(shareUser.getId());
		updshareUser.setStatus(shareUser.getStatus());
		shareUserRepository.save(updshareUser);
		return new ControllerResult();
	}
	
	@PostMapping("/getTemplateImgUrls")
	public ControllerResult getTemplateImgUrls(@RequestBody Map map) {	
		Assert.notNull(map.get("templateId"), ShzWebMsg.MSG_IDNULL);
		List<String> list =new ArrayList<>();
		TemplateUrlsVo templateUrlsVo =new TemplateUrlsVo();
		TemplateUrls templateUrls = templateUrlsRepository.findByTemplateId(Integer.valueOf(map.get("templateId").toString()));
		if(templateUrls != null && StringUtils.isNotEmpty(templateUrls.getUrls())) {
			templateUrlsVo.setMusicUrl(templateUrls.getMusicUrl());		
			String[] str = templateUrls.getUrls().split(";");
			for(String s : str) {
				list.add(s);
			}
 		}
		templateUrlsVo.setImgList(list);
		return new ControllerDataResult(templateUrlsVo);
	}
	
	
}
