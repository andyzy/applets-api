package com.shz.appletsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class AppletsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppletsApiApplication.class, args);
	}
}
